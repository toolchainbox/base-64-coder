FROM alpine:latest

RUN apk upgrade --no-cache && \
    apk add --no-cache coreutils

ENTRYPOINT ["base64"]
